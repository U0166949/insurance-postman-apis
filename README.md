# insurance-postman-apis

Este proyecto se creó con el objetivo de guardar y actualizar las diferentes apis de seguros que se pueden consumir desde postman

# Utilización

Lo que hay que hacer es importar el archivo **INSUREANCE.postman_collection.json** dentro de Postman.

Para hacer esto, primero hay que ejecutar Postman, luego ir a File > Import (Archivo > Importar).

Luego, dentro de la pestaña File, seleccionar la opción Upload Files y buscar el archivo **INSUREANCE.postman_collection.json**